//Main Carousel
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  myIndex++;
  if (myIndex > x.length) {
    myIndex = 1;
  }
  x[myIndex - 1].style.display = "block";
  setTimeout(carousel, 5000);
}

//Owl Carousel
$(document).ready(function () {
  $(".owl-carousel").owlCarousel({
    loop: true,
    items: 3,
    margin: 10,
    nav: false,
  });
});

//ScrollUp function

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    $("#scrollUpButton").addClass('show');
  } else {
    $("#scrollUpButton").removeClass('show');
  }
});

$("#scrollUpButton").on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});


